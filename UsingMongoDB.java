package com.training.personal;

import static com.mongodb.client.model.Filters.lte;

import java.util.HashMap;

import org.bson.Document;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;

public class UsingMongoDB {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		MongoClient client = MongoClients.create("mongodb://localhost:27017");
		
		MongoDatabase database = client.getDatabase("personal");

		MongoCollection<Document> collection = database.getCollection("personal_details");
		
		//MongoCursor<Document> cursor = collection.find().cursor();
		
//		while(cursor.hasNext()) {
//			
//			System.out.println(cursor.next().toJson());
//			
//		}
		
//		HashMap<String, Object> values = new HashMap<>();
//		values.put("name", "reshma");
//		
//		Document doc = new Document(values);
//		
//		collection.insertOne(doc);
//		
		FindIterable<Document> foundDoc= collection.find();
		
	//	FindIterable<Document> foundDoc= collection.find(lte("_id",102));
		
		for(Document eachDoc : foundDoc) {
			
			System.out.println(eachDoc.toJson());
		}


	}

}
